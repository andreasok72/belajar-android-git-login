package com.example.login12;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Pembelian extends AppCompatActivity {

    EditText edNamaBrg, edJumlahBrg, edHargaBrg, edKodeBrg;
    TextView tvNamaBrg, tvJumlah, tvHrg, tvKode;
    Button btnSimpan, btnHpsDt, btnKeluar, btnSelanjutnya;
    String NamaBrg, JumlahBrg, HargaBrg, KodeBrg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembelian);

        //EditText
        edNamaBrg = findViewById(R.id.edNamaBrg);
        edJumlahBrg = findViewById(R.id.edJumlahBrg);
        edHargaBrg = findViewById(R.id.edHargaBrg);
        edKodeBrg = findViewById(R.id.edKodeBrg);

        //TextView
        tvNamaBrg = findViewById(R.id.tvNamaBrg);
        tvJumlah = findViewById(R.id.tvJumlah);
        tvHrg = findViewById(R.id.tvHrg);
        tvKode = findViewById(R.id.tvKode);

        //Button
        btnSimpan = findViewById(R.id.btnSimpan);
        btnHpsDt = findViewById(R.id.btnHpsDt);
        btnKeluar = findViewById(R.id.btnKeluar);
        btnSelanjutnya =findViewById(R.id.btnNext2);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NamaBrg = edNamaBrg.getText().toString().trim();
                JumlahBrg = edJumlahBrg.getText().toString().trim();
                HargaBrg = edHargaBrg.getText().toString().trim();
                KodeBrg = edKodeBrg.getText().toString().trim();

                tvNamaBrg.setText(NamaBrg);
                tvJumlah.setText(JumlahBrg);
                tvHrg.setText(HargaBrg);
                tvKode.setText(KodeBrg);

        btnHpsDt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvNamaBrg.setText("");
                tvJumlah.setText("");
                tvHrg.setText("");
                tvKode.setText("");

                Toast.makeText(getApplicationContext(), "Data terhapus", Toast.LENGTH_SHORT).show();
            }
        });
                btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Pembelian.this, Penjualan.class));
                    }
                });

        btnKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Pembelian.this, HomeActivity.class));
            }
        });


            }
        });


    }
}