package com.example.login12;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    EditText judul, penulis, harga;
    Button input, update, hapus, lihat;
    DbHelper DB;
    ImageView btnBack;
    ImageView btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        judul = findViewById(R.id.judul);
        penulis = findViewById(R.id.penulis);
        harga = findViewById(R.id.harga);
        btnBack = findViewById(R.id.btnBack);
        btnNext = findViewById(R.id.btnNext);

        input = findViewById(R.id.btnInput);
        update = findViewById(R.id.btnUpdate);
        hapus = findViewById(R.id.btnHapus);
        lihat = findViewById(R.id.btnLihat);
        DB = new DbHelper(this);

        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String judulTXT = judul.getText().toString();
                String penulisTXT = penulis.getText().toString();
                String hargaTXT = harga.getText().toString();

                Boolean checkinsertdata = DB.insertuserdata(judulTXT, penulisTXT, hargaTXT);
                if (checkinsertdata == true)
                    Toast.makeText(HomeActivity.this, "Buku berhasil Ditambahkan", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(HomeActivity.this, "Buku Gagal Ditambahkan", Toast.LENGTH_SHORT).show();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String judulTXT = judul.getText().toString();
                String penulisTXT = penulis.getText().toString();
                String hargaTXT = harga.getText().toString();

                Boolean checkupdatedata = DB.updateuserdata(judulTXT, penulisTXT, hargaTXT);
                if (checkupdatedata == true)
                    Toast.makeText(HomeActivity.this, "Buku Berhasil Dupdate", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(HomeActivity.this, "Buku Gagal Diupdate", Toast.LENGTH_SHORT).show();
            }
        });

        hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String judulTXT = judul.getText().toString();
                Boolean checkudeletedata = DB.deletedata(judulTXT);
                if (checkudeletedata == true)
                    Toast.makeText(HomeActivity.this, "Data Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(HomeActivity.this, "Data Gagal Dihapus", Toast.LENGTH_SHORT).show();
            }
        });

        lihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor res = DB.getdata();
                if (res.getCount() == 0) {
                    Toast.makeText(HomeActivity.this, "Tidak Ada Buku Yang ditambahkan", Toast.LENGTH_SHORT).show();
                    return;
                }
                StringBuffer buffer = new StringBuffer();
                while (res.moveToNext()) {
                    buffer.append("Judul :" + res.getString(0) + "\n");
                    buffer.append("Penulis :" + res.getString(1) + "\n");
                    buffer.append("Harga :" + res.getString(2) + "\n\n");
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setCancelable(true);
                builder.setTitle("Data Buku");
                builder.setMessage(buffer.toString());
                builder.show();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, MainActivity.class));
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, Pembelian.class));
            }
        });

    }
}